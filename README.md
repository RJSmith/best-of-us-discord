# Best of Us Discord Bot

This is a simple moderation and command feature bot written in Python for the Discord server "Best of Us".

## Installation

This will be filled out later as the project is still in development.

## Current Features
1. Welcome Message - The discord bot will send a message to new users when they join the server.
2. EVE Online Time - Posts the current time in the game EVE Online.
3. BAH Calculator - Posts a link to the current Basic Allowance for Housing (BAH) calculator.
4. Discord Server Info - Provides the member count and a basic description of the server.
5. EVE Online Server Info - The bot will pull the server uptime, online players, and server version.
6. Invite Link - Provides the user with a sharable invite link.
7. Terminal Lance - Provides the URL for the Terminal Lance comic website.

## Planned Features in the Future

1. Remind Me Feature (Timer)
2. Random Dadjoke generator
3. Moderation Features
4. Random Terminal Lance comic.

Additional features will be added here.

## Contact, Support, and Contributing

I can be reached on the following platforms:
1. Discord: Alch#6096
2. Twitter: tupleHunden
3. Reddit: Alchemist8